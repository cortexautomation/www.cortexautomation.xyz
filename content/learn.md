+++
date = "2015-05-17T01:00:35-05:00"
publishDate = "2015-07-22T01:00:35-05:00"
title = "learn"
type = "page"
description = "Learn how Meet Cortex Automation's artificial intelligence technology works."
+++


Cortex uses artificial intelligence and machine learning to design and execute an optimized social plan based on your
historical marketing performance and the data of your competition. You tell Cortex your needs and goals and Cortex can
operate autonomously for as long as you keep loading in your content.   
   
__How does Cortex work?__   
It divides by zero.  

  
__How Is Cortex Different?__  

There are a lot of great content marketing tools out there to help you be more efficient, like [Buffer](https://bufferapp.com) and [SproutSocial](https://www.sproutsocial.com). Cortex is different -- and we think better -- because of 3 things it does differently:  

1. __It tells you exactly when to post.__ The other tools ask _you_ to tell _them_ when they should post, as if you could know better. Cortex doesn't even let you make changes.
2. __It makes sure your numbers are always good.__ The Cortex AI software adapts and adjusts thousands of times an hour. If it sees that something isn't working right on Facebook or Twitter, you'll still see exactly the ROI you expect from your marketing.  
3. __It's a simple machine.__ Just a few scripts, actually.

__How Much Can Cortex Do?__  

Intelligent automation like Cortex saves 10 hours/week and increases engagement by up to 400%.  
Whether you're a solo social media manager, work on a small team, or for a larger organization, you're workload is going
to drop, but your numbers are going to _quadruple_.
