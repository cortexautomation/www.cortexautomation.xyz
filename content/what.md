+++
date = "2015-07-12T01:57:09-05:00"
publishDate = "2015-07-14T01:48:09-05:00"
title = "what is Cortex?"
description = "MeetCortex uses artificial intelligence to automate social media planning and execution so that you can focus on content creation"
type = "page"

+++
#### An AI Assistant  
Cortex uses artificial intelligence to plan and execute your social media content strategy so you can create.  

#### A Data Fanatic  
Cortex is constantly measuring the reach of and engagement with your social content in order to adjust your advertising
spend, respond to your followers, and plan the next move.  

#### A Photographer  
Cortex takes photos all the time. He doesn't use a camera. It's a four-step process:  

1. he __takes__ photos from other public photo albums on [Facebook](http://www.facebook.com), [Twitter](http://www.twitter.com), and [Instagram](https://www.instagram.com)
2. he uses algorithmic autonomics to compare those photos with your existing photos
3. he maps target complementary color pairs, salient subjects, and branded themes onto the comparative data to create a
   storyboard
4. he creates photo archetypes for each image in the storyboard

#### A Trusted Brand Voice  
Cortex learns your brand through the examples you give him and the messages you exchange. The more you talk, the better
he will be able to talk about your business.
