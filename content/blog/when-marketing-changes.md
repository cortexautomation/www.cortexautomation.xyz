+++
date = "2016-01-12T11:06:40-05:00"
title = "when marketing changes"
by = "Matt Peters"
description = "Artificial Intelligence make marketers evolve fast."
tags = [ "technology", "trends", "marketing" ]
+++

When marketing changes marketers have to move fast. Or they'll get left behind. That's one of the reasons we built
Cortex: an artificially intelligent marketing tool is more vigilant and more able to react than a human. Don't believe
me? Here are a few times in the last month Cortex saved our butts.  

__Facebook Reactions__  

When Facebook [launched][1] these we were excited to see if people used them. And we figured we'd have to change our posting
strategy once we saw the data. But Cortex had already incorporated that into his recommendations, and we were
[suboptimized][2] from day 1.  

__Instagram's Algorithmic Feed__  

Cortex knew this was coming way before the [announcement][3] hit [TechCrunch][4]. We assumed Cortex would have to learn
the new algorithm after it went live, but we were wrong. Like magic, Cortex was already ready.




[1]: https://newsroom.fb.com/news/2016/02/reactions-now-available-globally/
[2]: http://www.cortexautomation.xyz/blog/the-true-cost-of-being-suboptimal/
[3]: https://techcrunch.com/2016/03/15/filteredgram/
[4]: https://techcrunch.com
