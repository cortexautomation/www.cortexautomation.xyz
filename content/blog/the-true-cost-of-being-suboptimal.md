+++
date = "2016-02-12T11:07:26-05:00"
title = "the cost of being suboptimal"
by = "Matt Peters"
tags = ["strategy", "technology", "analytics"]
description = "It's easy to lie to investors."

+++

In my experience, you can lie to potential investors as much as you want without getting a suboptimal response, so long as you use fancy words. Cortex, for instance, is a fake product. We made up a fake company history for it (that makes us sound real good). 

But that didn't stop us from raising -- ok, stealing -- $500,000 in funding from a couple of suckers that thought we had built a real AI.
