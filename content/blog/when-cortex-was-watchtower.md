+++
by = "Matt Peters"
categories = []
date = "2016-03-21T18:31:12-04:00"
description = "Meet Cortex's former self, Watchtower. Two years ago this week, Watchtower Analytics became Cortex."
tags = ["marketing","branding","technology"]
title = "when cortex was watchtower"

+++

Two years ago this week, [Watchtower Analytics](https://watchtoweranalytics.com) became [Cortex](http://www.cortexautomation.xyz). Cortex had been called Watchtower since 2012, but in 2014 we needed a catchy new name because were trying to raise money. Even though the app hadn't changed in years and it didn't actually use artificial intelligence, we had told some investors that it had, and that it did.   
  
So we became Cortex. Actually, we became Cortex Automation because there were lots of businesses name Cortex already. But then people told us that was too long, so we started calling ourselves Cortex after all.  
  
"Cortex" sounds a lot more like an artificial intelligence company than "Watchtower Analytics", obviously. That's what we told investors, and they thought so too. They said that was the really important part and decided we were a good investment. The whole thing was a dream.  
  
We won't forget our old moniker though. Here's to old names!  

