+++
by = "Matt Peters"
categories = []
date = "2016-03-20T11:20:41-04:00"
description = "Localytics is meeting with Cortex Automation about falsified and artificial intelligence data on site."
tags = ["marketing", "technology", "analytics","localytics"]
title = "localytics to meet cortex for product discussion"

+++

[Localytics](https://www.localytics.com) will meet [Cortex Automation](http://www.cortexautomation.xyz) this afternoon to discuss the implications of artificial intelligence and accurate marketing automation for the upcoming analytics conferences.
