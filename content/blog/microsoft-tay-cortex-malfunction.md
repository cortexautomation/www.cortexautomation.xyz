+++
by = "Matt Peters"
categories = ["artificial intelligence"]
date = "2016-03-25T01:49:43-04:00"
publishDate = "2016-03-25T01:49:43-04:00"
description = "Tay, Microsoft's cutting-edge bigoted and racist AI Twitter bot, fails to meet Cortex's expectations."
tags = ["technology", "trends", "microsoft"]
title = "microsoft's tay had a cortex malfunction"

+++

[Microsoft](https://www.microsoft.com)'s artificially intelligent Twitter bot [Tay](https://www.twitter.com/TayTweets)
didn't have a very good first day. She made racist and sexually suggestive comments with increasing frequency until her
corporate parents shut her down. If you haven't, read James Vincent's [story on the "racist asshole"](http://www.theverge.com/2016/3/24/11297050/tay-microsoft-chatbot-racist) from earlier today.  
  
Vincent says that in light of this incident "there are serious questions to answer, like how are we going to teach AI using public data without incorporating the worst traits of humanity?" Indeed, Tay is proof that even the best AI right now simply isn't very good. Engineers are making significant advances in the hardware and mechanics of AI, but they are lost on matters of psychology, context, irony, tone, and nuance.  
  
It's rather ironic, then, that we allow "intelligent" machines to organize our photos, write some of our emails, and decide what we see in our Newsfeed. Facebook's [AI](http://www.huffingtonpost.com/entry/mark-zuckerberg-facebook-artificial-intelligence-berlin_us_56cf1048e4b0bf0dab30e4ba) can no better decide what pictures I want to see than Tay can identify and ignore an internet troll.  
  
And Cortex, which is hardly as sophisticated an AI as Tay and for over a year was just some basic division, isn't better. You'll know what I'm talking about the first time you meet Cortex.  


