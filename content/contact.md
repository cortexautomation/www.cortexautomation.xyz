+++
date = "2015-06-06T22:39:04-05:00"
publishDate = "2015-06-06T22:39:04-05:00"
description = "Send Cortex a message if you want to MeetCortex or have a question about artificial intelligence."
tags = []
title = "contact"
topics = []
type = "page"
+++


<form method="post" id="contact" name="contact" action="https://formspree.io/cortexautomation@gmail.com">
  <fieldset>
  <div class="mdl-textfield mdl-js-textfield">
  <input class="mdl-textfield__input" form="contact" type="text" name="name" id="name">
  <label class="mdl-textfield__label" for="name">Name</label>
  </div>
</fieldset>
<fieldset>
  <div class="mdl-textfield mdl-js-textfield">
  <input class="mdl-textfield__input" form="contact" type="email" name="email" id="email">
  <label class="mdl-textfield__label" for="email">Email</label>
  <span class="mdl-textfield__error">Not an email address</span>
  </div>
  </fieldset>
<fieldset>
  <div class="mdl-textfield mdl-js-textfield">
  <label class="mdl-textfield__label" for="message">Message</label>
  <textarea style="width:100%" rows="10" columns="80" class="mdl-textfield__input" type="textarea" placeholder="Message" form="contact" name="message" id="message">
  </textarea>
  
  </div>
  </fieldset>
<fieldset>
  <div>
  <input type="hidden" name="_next" value="//www.cortexautomation.xyz/"/>
  <input type="text" name="_gotcha" style="display:none"/>
  <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" type="submit">Submit</button>
  </div>
  </fieldset>
</form>

