(function($) {
  $('.mdl-grid.home > .mdl-cell').delay(100);
  $('meta').each(function() {
    var $this = $(this);
    var $content = $this.attr('content');
    var $itemid = $this.attr('itemid');
    
    if($content && $content.match(/__baseurl__/)) {
      $this.attr('content', $content.replace(/__baseurl__/, window.location.host + '/'));
    }
    
    if($itemid && $itemid.match(/__baseurl__/)) {
      $this.attr('itemid', $itemid.replace(/__baseurl__/, window.location.host + '/'));
    }
  });
    $('[itemprop="url"]').each(function() {
      console.log($(this).attr('content'));
      $(this).attr('content', $(this).attr('content').replace('__baseurl__', window.location.host + '/'));
    });

})(jQuery);
